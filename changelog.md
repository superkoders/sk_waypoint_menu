### 0.0.8 / 14.04.2016
- přidán activeParent ve voláni closest() v activateLinks

### 0.0.7 / 06.04.2016
- fix předání this

### 0.0.6 / 06.04.2016
- číselné parametry **menuOffset, topOffset, bottomOffset, scrollOffset** nyní může být funkce s return. Lepší podpora pro responsivní varianty

### 0.0.5 / 11.02.2015
- přidání volání události **waypointtop** a **waypointbottom**. V parametrech události chodí směr a sekce

### 0.0.4 / 05.02.2015
- přidání volání události **beforeinit**
- po skončení animace scrollování se waypointy zapnou se spožděním 150ms. Tím se ošetřilo dodatečné volání, které někdy nastávalo.

### 0.0.3 / 05.02.2015
- oddělení historie

### 0.0.2 / 05.02.2015
- fix předávání reference na sekci uvnitř waypoint handleru
- závislost **jquery-waypoints** změněna na nezminifikovaný soubor

### 0.0.1 / 04.02.2015
- vytvoření
