$ = require('jquery')
Events = require('sk-events')
require('jquery-waypoints/lib/noframework.waypoints.js')
Waypoint = window.Waypoint

# Helper function
closest = (element, selector) ->
	unless selector
		return element
	return element.closest(selector)

getResult = (prop) ->
	if typeof prop is 'function'
		return prop()
	else
		return prop

class WaypointMenu extends Events
	constructor: (options) ->
		super

		@options = $.extend({
			context: window
			selector: '#menu-main a'
			menuOffset: ->
				return 130
			topOffset: 0
			bottomOffset: 0
			activeClass: 'is-active'
			activeParent: ''
			sectionClass: 'sk-section'
			scrollOffset: 0
			scrollDuration: 400
		}, options)

		@$D = $(document)
		@$W = $(window)
		@$scroll = $(@options.context)

		@isInit = false
		@isBusy = false

		@$links = if @options.selector.jquery then @options.selector else $(@options.selector)

	init: ->
		if @isInit or not @$links.length
			return this

		@actualWaypoints = []
		@activeWaypoints = []

		# Sections
		ids = @$links.map(-> $(@).prop('hash')).get()
		@$sections = $($.unique(ids).join(','))

		@trigger('beforeinit')

		# Click
		if @options.selector.jquery
			@$links.on('click', @handleClick)
		else
			@$D.on('click', @options.selector, @handleClick)

		# Waypoints
		@$sections
			.addClass(@options.sectionClass)
			.each((i, element) =>
				$element = $(element)
				waypoints =
					top: new Waypoint(
						element: element
						context: @$scroll.get(0)
						handler: (direction) =>
							@trigger('waypointtop',
								section: element
								direction: direction
							)
							if direction is 'down'
								@setActual(element)
							return
						offset: =>
							return getResult(@options.menuOffset) + getResult(@options.topOffset)
					)
					bottom: new Waypoint(
						element: element
						context: @$scroll.get(0)
						handler: (direction) =>
							@trigger('waypointbottom',
								section: element
								direction: direction
							)
							if direction is 'up'
								@setActual(element)
							return
						offset: =>
							-$element.height() + getResult(@options.menuOffset) + getResult(@options.bottomOffset)
					)

				$element.data('sk-waypoint-menu', waypoints)
			)


		@trigger('init')
		@isInit = true
		return this

	destroy: ->
		unless @isInit
			return this

		if @options.selector.jquery
			@$links.off('click', @handleClick)
		else
			@$D.off('click', @options.selector, @handleClick)

		@$links.removeClass(@options.activeClass)

		# Waypoints
		@$sections
			.removeClass(@options.sectionClass)
			.each((i, element) ->
				$element = $(element)
				waypoints = $element.data('sk-waypoint-menu')
				waypoints.top.destroy()
				waypoints.bottom.destroy()
				$element.removeData('sk-waypoint-menu')
			)

		@trigger('destroy')
		@isInit = false
		return this

	handleClick: (event) =>
		event.preventDefault()

		hash = $(event.currentTarget).prop('hash').slice(1)
		@scrollTo('#' + hash)

		return

	scrollTo: (hash) ->
		# Target to scroll
		$target = $(hash)
		scrollTop = @$scroll.scrollTop()
		top = $target.offset().top

		if @$scroll.get(0) isnt window
			top += scrollTop

		top = Math.ceil(top)

		# Actual
		@setActual($target.get(0), -1)
		# Set busy
		@isBusy = true
		@anim and @anim.stop()
		@anim =
			$.Animation(
				top: scrollTop
			,
				top: top - getResult(@options.menuOffset) + getResult(@options.scrollOffset)
			,
				duration: @options.scrollDuration
			)
				.progress((obj) =>
					@$scroll.scrollTop(obj.elem.top)
					return
				)
				.done(=>
					setTimeout(=>
						# off busy state
						@isBusy = false
						# Actual
						@setActual($target.get(0), 1)
					, 150)

				)

		return

	activateLinks: (sections) ->
		selector = '[href="' + sections.join('"], [href="') + '"]'

		closest(@$links.not(selector), @options.activeParent).removeClass(@options.activeClass)
		closest(@$links.filter(selector), @options.activeParent).addClass(@options.activeClass)

		return

	getSections: (element) ->
		parents = $(element)
			.parents('.' + @options.sectionClass)
			.map(->
				'#' + @id
			).get()

		return ['#' + element.id].concat(parents)

	setActual: (element, isScroll) ->
		waypoints = @getSections(element)

		if waypoints.join(',') isnt @actualWaypoints.join(',') or isScroll is 1
			if isScroll is -1
				@activateLinks(waypoints)
				return
			# Before
			unless @isBusy
				@trigger('beforechange')
			# Actual
			@actualWaypoints = waypoints
			# Active
			unless @isBusy
				@activeWaypoints = waypoints
				@activateLinks(waypoints)
				@trigger('change')
		return


module.exports = WaypointMenu
